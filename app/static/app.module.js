/**
 * Application configuration and initialization.
 */
(function() {
    'use strict';

    angular
        .module('app', [
            'ui.router',
            'ui.checkbox',
            'flaskular.router',
            'flaskular.layout',         // Extended
            'flaskular.home',           // Extended
            // 'flaskular.dashboard',   // Replaced by a custom module
            'flaskular.auth',
            'flaskular.profile',
            'app.home',
            'app.layout',
            'app.dashboard'
        ])
        .config(configure)
        .run(runBlock);

    configure.$inject =
        [];

    runBlock.$inject =
        ['routerHelper', 'authService', '$rootScope', '$state'];


    /**
     * App configuration.
     *
     * @namespace app
     */
    function configure() {

    }

    /**
     * App initialization.
     *
     * @namespace app
     */
    function runBlock(routerHelper, authService, $rootScope, $state) {
        function handleLogin(data) {
            routerHelper.initialize();
        }

        // Configure authentication
        authService.loginIfCurrent()
            .then(handleLogin)
            .catch(handleLogin);
    }

})();
