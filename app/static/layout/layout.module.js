/**
 * Module definition for layout.
 *
 * @namespace iapp.layout
 */
(function(){
    'use strict';

    angular
        .module('app.layout', [
            'app.layout.topnav'
        ]);
    angular
        .module('app.layout.topnav', [
            'flaskular.layout.topnav',
            'flaskular.auth'
        ]);
})();
