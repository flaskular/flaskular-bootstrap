/**
 * Directive for defining the top navigation. We will not use the core
 * flaskular directive, but we will use the core controller.
 *
 * Usage is <topnavext></topnavext>
 *
 * @namespace app.layout.topnav
 */
(function() {
    'use strict';

    angular
        .module('app.layout.topnav')
        .controller('ExtTopnavController', ExtTopnavController)
        .directive('topnavext', topnav);

    ExtTopnavController.$inject =
        ['$scope', '$state', '$controller', 'authService', 'topnavService'];

    ///////////////////////////////////////////////////////////////////////////

    /**
     * topnav directive definition.
     */
    function topnav() {
        return {
            restrict: 'E',
            templateUrl: 'static/layout/topnav.directive.html',
            controller: ExtTopnavController,
            controllerAs: 'vm',
            bindToController: true
        };
    }

    function ExtTopnavController($scope, $state, $controller, authService,
                                 topnavService) {
       var base = $controller('TopnavController', {
           $scope: $scope, $state: $state, authService: authService,
           topnavService: topnavService
       });
       var vm = base.extendTo(this);
    }
})();
