from flask import render_template
from flaskular.angular.controllers import angular

from .models import list_all_static_js, list_all_static_css


###############################################################################
#   Routes
###############################################################################

@angular.route('/')
def index():
    """Overwrites the index route of flaskular in order to get all app css/js.

    Angnular takes over from here, making calls back to flask as necessary.
    """
    print(list_all_static_css())
    return render_template('angular/index.html',
                           static_js=list_all_static_js(),
                           static_css=list_all_static_css())
