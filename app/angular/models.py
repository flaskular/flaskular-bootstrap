from flask import current_app

from flaskular.angular.models import (
    list_all_static_css as flaskular_list_css,
    list_all_static_js as flaskular_list_js
)


def list_all_static_css():
    """Auto-generates a list of all static css files.

    These are files that need to be included in the html template.

    Returns
    -------
    css_lst : lst of string
        A list of all filenames of css files, relative to static.
    """
    return flaskular_list_css(current_app._static_folder, __file__)


def list_all_static_js():
    """Lists all js scripts in the flaskular directory.

    Modules first, and then the rest.

    Returns
    -------
    js_list : lst of string
        A list of all filenames of js files, relative to static/ but excluding
        the test *.spec.js.
    """
    return flaskular_list_js(current_app._static_folder, __file__)
