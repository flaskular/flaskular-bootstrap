from flaskular.extensions import (db,               # noqa
                                  security,         # noqa
                                  print_warning,    # noqa
                                  print_error,      # noqa
                                  print_ok)         # noqa


# Add any custom extensions here
# ...
