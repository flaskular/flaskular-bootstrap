"""
Manages the database migrations using Alembic and Flask-Migrate. See README.md
for usage instructions.
"""
from flaskular_manage.manage import build_manager
from app.factory import create_app, db


if __name__ == '__main__':
    app = create_app()
    build_manager(app, db).run()
