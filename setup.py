from setuptools import setup

from flaskular_manage import (FlaskularInstall, FlaskularDevelop,
                              FlaskularPyTest, FlaskularPyDoc)


class Install(FlaskularInstall):

    def run(self):
        FlaskularInstall.run(self)
        # Put any custom installation scripts here


class Develop(FlaskularDevelop):

    def run(self):
        FlaskularDevelop.run(self)
        # Put any custom develop scripts here


class PyTest(FlaskularPyTest):

    project_dir = 'app'

    def initialize_options(self):
        # Put any custom initialization scripts here
        FlaskularPyTest.initialize_options(self)

    def finalize_options(self):
        FlaskularPyTest.finalize_options(self)
        # Put any custom finalization scripts here

    def run_tests(self):
        # Put any custom run scripts here
        FlaskularPyTest.run_tests(self)


class PyDoc(FlaskularPyDoc):
    description = 'run documentation'
    user_options = []

    def initialize_options(self):
        # Put any custom initialization scripts here
        FlaskularPyDoc.initialize_options(self)

    def finalize_options(self):
        # Put any custom finalization scripts here
        FlaskularPyDoc.finalize_options(self)

    def run(self):
        FlaskularPyDoc.run(self)

setup(name='flaskular-bootstrap',
      version='0.1.0',
      packages=['app'],
      include_package_data=True,
      install_requires=[
          # Put any dependencies this project requires above those required by
          # flaskular
          'gevent'
      ],
      tests_require=[
          # Put any test dependencies this project requires, including those
          # required by flaskular (just in case tests aren't run there)
          'pytest',
          'pytest-cov',
      ],
      cmdclass={
          'install': Install,
          'develop': Develop,
          'test': PyTest,
          'document': PyDoc
      })
